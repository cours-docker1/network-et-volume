# Network et volume


## Exercice : Comprendre le Réseau Docker avec BusyBox

## Objectif

L'objectif de cet exercice est de comprendre comment les réseaux Docker fonctionnent et comment les conteneurs peuvent communiquer entre eux en utilisant des noms DNS et des adresses IP spécifiques. Vous allez créer plusieurs conteneurs busybox, les attacher à différents réseaux Docker, et tester la connectivité entre eux en utilisant des outils réseau tels que ping et curl.

# Contexte

Docker utilise des réseaux pour permettre la communication entre les conteneurs. Par défaut, Docker crée un réseau bridge auquel tous les conteneurs sont connectés. Vous pouvez également créer des réseaux personnalisés pour une meilleure isolation et une gestion plus fine des adresses IP.

Dans cet exercice, vous allez :

Créer un réseau Docker personnalisé.
Lancer des conteneurs avec l'image `busybox` avec des noms DNS et des adresses IP spécifiques.
Vérifier la connectivité entre les conteneurs sur le même réseau.
Vérifier la connectivité entre les conteneurs sur des réseaux différents.
### Étape 1 : Créer un réseau Docker personnalisé

Créez un réseau Docker personnalisé nommé mynetwork avec un sous-réseau spécifique :

- La plage ip doit être `172.72.0.0/16`
- Le nom du réseau doit être `mynetwork`

```sh
docker network create --subnet=<range_ip> <network_name>
# Cette commande crée un réseau Docker personnalisé avec un sous-réseau. Les conteneurs attachés à ce réseau auront des adresses IP dans cette plage.
```

### Étape 2 : Lancer des conteneurs avec des noms DNS et des adresses IP spécifiques

Lancez deux conteneurs `busybox` sur le réseau `mynetwork` avec des noms DNS et des adresses IP spécifiques :

```sh
docker run -d --name <container_name> --network <network_name> --hostname <dns_name> --ip <ip> busybox sleep 3600
# Exemple ip : 172.72.0.2
```
Lancez un troisième conteneur `busybox` en dehors du réseau personnalisé :

```sh
docker run -d --name <container_name> busybox sleep 3600
```

### Étape 3 : Vérifier la connectivité réseau

Vérifier la connectivité entre les deux containers du réseau `mynetwork`

Connectez-vous sur l'un des conteneurs et utilisez `ping` pour tester la connectivité avec le second :

```sh
ping <dns_name_or_ip>
```

# Tester la connectivité avec le troisième conteneur (qui est dans un réseau différent)

connectez-vous sur le troisième conteneur et utilisez `ping` pour tester la connectivité sur l'un des conteneurs qui se trouve sur `mynetwork`


# Conclusion
Cet exercice montre comment Docker gère les réseaux et la communication entre les conteneurs. En utilisant des réseaux personnalisés, vous pouvez isoler les conteneurs et contrôler plus finement la connectivité réseau. Vous avez appris à :

Créer un réseau Docker personnalisé.
Lancer des conteneurs avec des noms DNS et des adresses IP spécifiques.
Vérifier la connectivité entre les conteneurs sur le même réseau.
Vérifier la connectivité entre les conteneurs sur des réseaux différents.
Ces compétences sont essentielles pour gérer efficacement les environnements conteneurisés et garantir la sécurité et l'isolation des applications.

## commandes utiles

pour connaitre les réseaux existants :

```sh
docker network ls
```

pour connaitre le réseau correspondant du conteneur:

```sh
docker inspect <id_or_name>
# un énorme json va sortir, ce qui peut être pénible pour recherche l'information correspondant.
docker inspect <id_or_name> | grep NetworkMode
# on va récupérer directement le nom du réseau sur lequel le conteneur est associé
```

# Volumes

## Objectif

L'objectif de cet exercice est de comprendre comment les volumes Docker fonctionnent et comment ils permettent de persister des données au-delà de la durée de vie d'un conteneur. Vous allez créer plusieurs conteneurs busybox, utiliser des volumes pour partager des données entre eux, et vérifier que les données persistent même après la suppression des conteneurs.

## Contexte

Docker utilise des volumes pour stocker des données en dehors du système de fichiers des conteneurs. Cela permet de partager des données entre plusieurs conteneurs et de persister des données indépendamment du cycle de vie des conteneurs. Les volumes Docker sont stockés sur l'hôte Docker et peuvent être montés dans un ou plusieurs conteneurs.

Dans cet exercice, vous allez :

Créer un volume Docker.
Lancer des conteneurs busybox et monter le volume.
Ajouter des données au volume depuis un conteneur.
Accéder aux données depuis un autre conteneur.
Vérifier que les données persistent après la suppression des conteneurs.

## Étape 1 : Lancer un container nginx

Pour comprendre la persistance de données, je vais vous demander de lancer un container en détaché `nginx`, avec le port hôte sur `8080` et le port du conteneur nginx sur `80`.

Accédez ensuite à l'application depuis votre navigateur à l'aide de l'adresse IP externe de votre VM et du port que vous avez déclaré.

Une fois dans le conteneur Nginx, dirigez vous sur le répertoire `/usr/share/nginx/html`

## Étape 2 : Modifier le fichier index.html

Une fois dans le conteneur Nginx, dirigez vous sur le répertoire `/usr/share/nginx/html`

Dans un conteneur nginx, il est pas possible d'utiliser nano|vim|vi, vous allez donc l'installer en utilisant la commande :

```bash
apt update
```

puis : 

```bash
apt install nano|vim|vi -y
```

et éditez le fichier `index.html`

par exemple comme ceci : 

```html
<!DOCTYPE html>
<html>
<head>
    <title>Welcome to league of draaaaaaaaaven !</title>
    <style>
        body {
            width: 35em;
            margin: 0 auto;
            font-family: Tahoma, Verdana, Arial, sans-serif;
        }
    </style>
</head>
<body>
    <h1>Welcome to league of draaaaaaaaaven !</h1>
    <p>If you see this page, the nginx web server is successfully installed and working. Further configuration is required.</p>
    <p>For online documentation and support please refer to <a href="http://nginx.org/">nginx.org</a>.</p>
    <p><em>Thank you for using nginx.</em></p>
</body>
</html>
```

De nouveau, vous pouvez accéder au nouveau contenu depuis votre navigateur !

## Étape 3 : Vérifiez la persistance 

pour vérifier que le changement du contenu `nginx` est persistant.

Supprimez le container `nginx`.

Ensuite relancer le de nouveau et accèdez au navigateur pour vous rendre compte que vos changements ne sont plus là !

## Étape 4 : Créer un volume Docker

Créez un volume Docker nommé `myvolume` :

```sh
docker volume create <volume_name>
```

## Étape 5 : Lancer des conteneurs et monter le volume
Lancez un conteneur `busybox`, nommez le `container1` et montez le volume myvolume sur le répertoire `/data` du conteneur :

```sh
docker run --name <container_name> --mount source=<volume_name>,target=<path_to_my_data> busybox sleep 3600
```

## Étape 6 : Ajouter des données au volume

Connectez-vous à container1 et ajoutez des données au volume (sur le répertoire `/data`) :

```sh
docker exec -it <container_name> sh
```

## Étape 7 : Ajouter des données au volume

```sh
echo "Hello from container1" > /data/hello.txt
```

## Étape 8 : Accéder aux données depuis un autre conteneur

Lancez un autre conteneur `busybox`, nommez le `container2` et montez sur ce même volume `myvolume` dans le répertoire `/data` :

```sh
docker run -it --name <container_name> --mount source=<volume_name>,target=<path_to_my_data> busybox
```

Connectez-vous à container2 et vérifiez les données dans le volume :

```sh
docker exec -it <container_name> sh
```

Accéder aux données dans le volume

```sh
cat /data/hello.txt
```

Vous devriez voir le message "Hello from container1", confirmant que les données dans le volume sont accessibles depuis plusieurs conteneurs.


## Étape 9 : Vérifier la persistance des données

Supprimez les conteneurs `container1` et `container2`.

```sh
docker rm -f container1 container2
```

Lancez un nouveau conteneur `busybox`, nommez le `container3` et montez le volume `myvolume` :

```sh
docker run --name <container_name> --mount source=<volume_name>,target=<path_to_my_data> busybox
```

Connectez-vous à `container3` et vérifiez les données dans le volume :

```sh
docker exec -it container3 sh
```

## Étape 10 : Accéder aux données dans le volume

```sh
cat /data/hello.txt
```

Vous devriez toujours voir le message "Hello from container1", confirmant que les données dans le volume persistent même après la suppression des conteneurs.

Cet exercice montre comment Docker gère les volumes et la persistance des données. En utilisant des volumes, vous pouvez partager des données entre plusieurs conteneurs et les rendre persistantes indépendamment du cycle de vie des conteneurs. Vous avez appris à :

- Créer un volume Docker.

- Monter un volume dans des conteneurs.

- Ajouter et accéder à des données dans un volume.

- Vérifier la persistance des données après la suppression des conteneurs.

Ces compétences sont essentielles pour gérer efficacement les données dans des environnements conteneurisés.

**ou se stock le volume ?**

Les volumes Docker sont stockés sur le système de fichiers de l'hôte Docker. Par défaut, Docker gère automatiquement l'emplacement des volumes, mais vous pouvez également spécifier un emplacement personnalisé si nécessaire. Voici quelques détails sur où se trouvent les volumes Docker par défaut et comment vous pouvez les gérer.

## Emplacement par défaut des volumes Docker

Sur un système Linux, les volumes Docker sont généralement stockés dans le répertoire suivant :


**/var/lib/docker/volumes/**

Chaque volume a son propre sous-répertoire dans ce répertoire. Le contenu de ce sous-répertoire correspond aux données du volume.

## Afficher l'emplacement d'un volume spécifique

Pour afficher des informations détaillées sur un volume spécifique, y compris son emplacement sur le système de fichiers de l'hôte, vous pouvez utiliser la commande docker volume inspect :

```sh
docker volume inspect <volume_name>
```

Supposons que vous avez un volume nommé myvolume. Voici comment vous pouvez inspecter ce volume et voir son emplacement :

```sh
docker volume inspect myvolume
```

Vous verrez une sortie similaire à celle-ci :

```json
[
    {
        "CreatedAt": "2024-06-23T12:34:56Z",
        "Driver": "local",
        "Labels": {},
        "Mountpoint": "/var/lib/docker/volumes/myvolume/_data",
        "Name": "myvolume",
        "Options": {},
        "Scope": "local"
    }
]
```

Dans ce cas, le volume myvolume est stocké dans le répertoire **/var/lib/docker/volumes/myvolume/_data** sur l'hôte.

## Accéder aux données d'un volume sur l'hôte

Vous pouvez accéder aux données d'un volume directement depuis l'hôte Docker en naviguant vers le répertoire correspondant. Par exemple, pour myvolume :

```sh
cd /var/lib/docker/volumes/myvolume/_data
```
Vous trouverez ici les fichiers et répertoires stockés dans le volume.

Spécifier un emplacement personnalisé pour un volume
Si vous souhaitez spécifier un emplacement personnalisé pour un volume, vous pouvez le faire en utilisant l'option --mount lors de la création d'un conteneur. Par exemple :

```sh
docker run -it --name <container_name> --mount type=bind,source=</path/to/host/directory>,target=/data busybox
```

Dans cet exemple, le répertoire `/path/to/host/directory` sur l'hôte est monté dans le conteneur à l'emplacement `/data`. Notez que ceci est une liaison de montage (bind mount), qui est différente d'un volume Docker standard, mais elle permet un contrôle total sur l'emplacement du stockage.

# Conclusion
Les volumes Docker sont stockés sur le système de fichiers de l'hôte, généralement dans le répertoire `/var/lib/docker/volumes/`. Vous pouvez inspecter les volumes pour voir leur emplacement exact et accéder directement aux données sur l'hôte. Vous avez également la possibilité de spécifier des emplacements personnalisés pour les volumes en utilisant des liaisons de montage.